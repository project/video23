<?php

/**
 * @file video23_embed.tpl.php
 * 
 * Provides a simple placeholder used to embed the video via JavaScript.
 *
 * Available variables:
 * - $photo_id: Video 23's ID for the video.
 * - $token: Embedding token used to get playback access.
 * - $width: The width value for the placeholder.
 * - $height: The height value for the placeholder.
 */
?>
<div id="video23-embed-<?php print $photo_id; ?>" class="video23-embed">
</div>

