<?php

/**
 * @file video23_upload.install
 */

/**
 * Implementation of hook_install().
 */
function video23_upload_install() {
  drupal_install_schema('video23_upload');
}

/**
 * Implementation of hook_uninstall().
 */
function video23_upload_uninstall() {
  drupal_uninstall_schema('video23_upload');
}

/**
 * Implementation of hook_schema().
 */
function video23_upload_schema() {
  $schema = array();

  $schema['video23_upload'] = array(
    'description' => 'Keep track of videos uploaded to 23 Video',
    'fields' => array(
      'upload_id' => array(
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'description' => 'Unique identifier for the {video23_upload}.',
      ),
      'owner_type' => array(
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'description' => 'Owner type, usually the name of the module that handled the file upload.',
      ),
      'owner_name' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
        'description' => 'Owner name, like the name of the CCK field used or similar.',
      ),
      'owner_id' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => 'Foreign key to the object the video is directly tied to, usually a user or a node.',
      ),
      'delta' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'For owners that have more than one video per id (like CCK fields), the delta value can be used to distinguish them.',
      ),
      'fid' => array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => FALSE,
        'description' => '{files}.fid for the uploaded file as long as it is on our server.',
      ),
      'status' => array(
        'type' => 'varchar',
        'length' => 64,
        'default' => 'pending',
        'description' => 'The current status of the upload. The flow is pending -> uploaded -> ready – or failed, if something bad happens.',
      ),
      'photo_id' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
        'description' => 'The "photo_id" 23 Video assigns to the video on upload.',
      ),
      'token' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
        'description' => 'The token assigned to the video by 23 Video.',
      ),
      'name' => array(
        'type' => 'varchar',
        'length' => 255,
        'default' => '',
        'description' => 'The name given to the video when uploaded to 23 Video.'
      )
    ),
    'primary key' => array('upload_id'),
    'indexes' => array(
      'owner_type_id' => array('owner_type', 'owner_id'),
      'owner_type_name_id' => array('owner_type', 'owner_id'),
      'fid' => array('fid'),
      'photo_id' => array('photo_id'),
    ),
  );

  return $schema;
}