<?php

/**
 * @file video23_upload.admin.inc
 * Admin settings for the 23 Video upload module.
 */

/**
 * Main settings form.
 */
function video23_upload_admin_settings_form(&$form_state) {
  $form = array();

  $form['video_23_upload_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Upload path'),
    '#description' => t('Path where the uploaded video files are stored before being sent along to 23 Video. Must be writeable. Ideally, somewhere outside your webroot with lots of available disk space. If a relative path is specified, it will be considered relative to the !link.', array('!link' => l(t('default file system path'), 'admin/settings/file-system'))),
    '#required' => TRUE,
    '#default_value' => variable_get('video_23_upload_path', 'video_23_upload'),
  );

  return system_settings_form($form);
}

function video23_upload_admin_settings_form_validate($form, &$form_state) {
  $dir = $orig_path = trim($form_state['values']['video_23_upload_path']);

  // If not an absolute path, prepend Drupal's file system path.
  if (strpos($dir, '/') !== 0) {
    $dir = variable_get('file_directory_path', 'sites/default/files') . '/' . $dir;
  }

  if (file_check_directory($dir, TRUE, 'video_23_upload_path')) {
    form_set_value($form['video_23_upload_path'], $orig_path, $form_state);
  }
}

