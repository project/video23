<?php

/**
 * @file
 * Hooks provided by the Video 23 module.
 */

/**
 * Called when a video has successfully been uploaded to the 23video server.
 *
 * This hook is used to let modules "listen" to video23_upload and act when
 * a video has been uploaded. If the uploaded video is of interest, it is
 * possible use data returned from 23 Video.
 *
 * @param string $owner_type
 *      Usually the name of the module that handled the file upload.
 * @param integer $owner_id
 *      Id of the owner, fx a node or user.
 * @param integer $fid
 *      The file id of the video.
 * @param string $photo_id
 *      The photo_id assigned by 23 Video to the uploaded video.
 * @param string $token
 *      The token assinged by 23 Video to the uploaded video.
 */
function hook_video23_video_upload($owner_type, $owner_id, $fid, $photo_id, $token) {
  
}

/**
 * Example implementation of hook_video23_video_upload()
 *
 * In this example we want to assign the 23 Video data to a custom table.
 */
function example_video23_video_upload($owner_type, $owner_id, $fid, $photo_id, $token) {
  if ($owner_type == 'example') {
    db_query("UPDATE {example_table} SET photo_id = '%s', token = '%s'
              WHERE example_id = %d
              LIMIT 1", $photo_id, $token, $owner_id);
  }
}