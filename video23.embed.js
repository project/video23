
/**
 * @file video23.embed.js
 * Handles the embedding of videos via jQuery FlashEmbed.
 */

Drupal.behaviors.video23Embed = function () {
  // Embed each video.
	$.each(Drupal.settings.video23.embed, function (i, video) {
    var container = $("#video23-embed-" + video.photo_id);

    if (video.width) {
      container.css('width', video.width);
    }
    if (video.height) {
      container.css('height', video.height);
    }

    container.flashembed(Drupal.settings.video23.url + '/v.swf', {
      token: video.token,
      photo_id: video.photo_id,
    });
  });
}

