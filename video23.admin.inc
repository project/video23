<?php

/**
 * @file video23.admin.inc
 * Admin settings for the 23 Video module.
 */

/**
 * Main admin settings form.
 */
function video23_admin_form($form_state) {
  $form = array();

  $form['video23_site_url'] = array(
    '#type' => 'textfield',
    '#title' => t('23 Video site URL'),
    '#description' => t('URL to your 23 Video site, e.g. http://example.23video.com/'),
    '#required' => TRUE,
    '#default_value' => variable_get('video23_site_url', ''),
  );

  $form['video23_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#description' => t('To work with 23 Video, Drupal needs the secret API key.'),
    '#required' => TRUE,
    '#default_value' => variable_get('video23_api_key', ''),
  );

  $form['dimensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Video dimensions'),
    '#description' => t('The dimensions of the video when displayed. These values are applied via inline CSS. A value of 0 will cause that dimension not to be set on the output. That can be helpful if you are trying to set the dimensions in your stylesheet, but note that if the size of the output div is not set anywhere, the video will not appear.'),
  );

  $form['dimensions']['video23_default_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#default_value' => variable_get('video23_default_width', 625),
    '#size' => 3,
    '#prefix' => '<div class="container-inline">',
  );

  $form['dimensions']['video23_default_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#default_value' => variable_get('video23_default_height', 469),
    '#size' => 3,
    '#suffix' => '</div>'
  );
  return system_settings_form($form);
}

/**
 * Validation for the admin settings form.
 */
function video23_admin_form_validate($form, &$form_state) {
  $url = filter_var(rtrim(drupal_strtolower($form_state['values']['video23_site_url']), '/'), FILTER_SANITIZE_URL);
  if (!filter_var($url, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED & FILTER_FLAG_HOST_REQUIRED) || strpos($url, 'http') !== 0) {
    form_set_error('video23_site_url', t('The value you entered was not a valid URL.'));
  }
  else {
    // Check if we can access the 23 Video API with the provided URL and key.
    $key = trim($form_state['values']['video23_api_key']);

    $status = video23_connection_check($url, $key);
    if ($status == VIDEO23_CONNECTION_OK) {
      form_set_value($form['video23_site_url'], $url, $form_state);
      form_set_value($form['video23_api_key'], $key, $form_state);
    }
    else {
      $message = _video23_status_message($status);
      if (in_array($status, array(VIDEO23_INVALID_SIGNATURE, VIDEO23_ACCESS_DENIED))) {
        $element_name = 'video23_api_key';
      }
      else {
        $element_name = 'video23_site_url';
      }

      form_set_error($element_name, $message['description']);
    }
  }

  // Invalid or empty values are stored as 0.
  if (empty($form_state['values']['video23_default_width']) || !is_numeric($form_state['values']['video23_default_width'])) {
    $form_state['values']['video23_default_width'] = 0;
  }
  if (empty($form_state['values']['video23_default_height']) || !is_numeric($form_state['values']['video23_default_height'])) {
    $form_state['values']['video23_default_height'] = 0;
  }
}

